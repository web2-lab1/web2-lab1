import './App.css';
import { Route, Routes } from 'react-router-dom';
import CompetitionForm from './components/CompetitionForm';
import CompetitionView from './components/CompetitionView';
import HomePage from './components/HomePage';

function App() {

  return (
    <Routes>
      <Route path="/" exact element={<HomePage />} />
      <Route path="/newCompetition" element={<CompetitionForm />} />
      <Route path="/competition/:natjecanjeid" element={<CompetitionView />} />
    </Routes>
  );
}

export default App;
