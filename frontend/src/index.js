import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import { Auth0Provider } from '@auth0/auth0-react';
import Header from './components/Header';
import { BrowserRouter } from 'react-router-dom';
<link rel="icon" href="/favicon.ico" />

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Auth0Provider
      domain="unamaravic.eu.auth0.com"
      clientId="cU7QUg0nTN00ErV2oovTE86ASFuHOL5r"
      authorizationParams={{
        redirect_uri: 'https://web2-lab1-front-e9ew.onrender.com'
      }}
      audience="uniqueidentifier"
      scope="openid profile email"
    >
      <BrowserRouter>
        <Header />
        <App />
      </BrowserRouter>
    </Auth0Provider>
  </React.StrictMode>
);

