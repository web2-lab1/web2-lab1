import React from 'react';
import { Typography, Button } from '@mui/material';
import { useAuth0 } from '@auth0/auth0-react';

const HomePage = () => {

    const { loginWithRedirect } = useAuth0();


    return (
        <div style={{ textAlign: 'center', marginTop: '50px' }}>
            <Typography variant="h2" gutterBottom>
                Dobrodošli na početnu stranicu
            </Typography>
            <Typography variant="body1" paragraph>
                Ovdje možete stvoriti novo natjecanje ili pregledati postojeće.
            </Typography>
            <Button variant="contained" color="primary" onClick={loginWithRedirect}>
                Prijavite se
            </Button>
        </div>
    );
};

export default HomePage;
