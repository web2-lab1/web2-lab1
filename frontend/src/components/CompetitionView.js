import React, { useState, useEffect } from 'react';
import Poredak from './Poredak';
import Raspored from './Raspored';
import Axios from 'axios';
import { Button, Typography, Grid } from '@mui/material';
import { useParams } from 'react-router-dom';
import ResultsAdmin from './ResultsAdmin';

const CompetitionView = () => {
  const { natjecanjeid } = useParams();
  const [selectedComponent, setSelectedComponent] = useState(null);
  const [nazivNatjecanja, setNazivNatjecanja] = useState('');

  useEffect(() => {
    const fetchNazivNatjecanja = async () => {
      try {
        console.log(natjecanjeid)
        const response = await Axios.get('https://web2-lab1-p5fl.onrender.com:8080/create', { params: { natjecanjeid } });
        console.log(response.data);
        setNazivNatjecanja(response.data["naziv"]);
      } catch (error) {
        console.error('Greška pri dohvaćanju imena natjecanja:', error);
      }
    };

    fetchNazivNatjecanja();
  }, []);

  const handleButtonClick = (component) => {
    setSelectedComponent(component);
  };

  return (
    <div>
      <br />
      <Typography variant="h4" align="center" gutterBottom>
        {nazivNatjecanja}
      </Typography>
      <Grid container spacing={2} justifyContent="center">
        <Grid item>
          <Button variant="contained" color="primary" onClick={() => handleButtonClick(<Poredak natjecanjeid={natjecanjeid} />)}>
            Pogledaj trenutni poredak
          </Button>
        </Grid>
        <Grid item>
          <Button variant="contained" color="primary" onClick={() => handleButtonClick(<ResultsAdmin natjecanjeid={natjecanjeid} />)}>
            Pogledaj rezultate
          </Button>
        </Grid>
        <Grid item>
          <Button variant="contained" color="primary" onClick={() => handleButtonClick(<Raspored natjecanjeid={natjecanjeid} />)}>
            Pogledaj raspored
          </Button>
        </Grid>
      </Grid>
      {selectedComponent}
    </div>
  );
};

export default CompetitionView;
