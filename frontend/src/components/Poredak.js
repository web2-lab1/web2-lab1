import React, { useState, useEffect } from 'react';
import { Table, TableContainer, TableHead, TableRow, TableCell, TableBody, Paper, Typography } from '@mui/material';
import Axios from 'axios';

const Poredak = ({natjecanjeid}) => {
  const [standings, setStandings] = useState([]);
  const [naziv, setNaziv] = useState('');

  useEffect(() => {
    const fetchStandings = async () => {
      try {
        const response = await Axios.get('https://web2-lab1-p5fl.onrender.com:8080/table', { params: { natjecanjeid } });
        setStandings(response.data["poredak"]);
        setNaziv(response.data["naziv"]["naziv"]);
      } catch (error) {
        console.error('Greška pri dohvaćanju tablice poretka:', error);
      }
    };

    fetchStandings();
  }, []);

  return (
    <TableContainer component={Paper}>
      <br /><br />
  	  <Typography variant="h4" align="center" gutterBottom>Poredak</Typography>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Rbr</TableCell>
            <TableCell>Naziv</TableCell>
            <TableCell>Trenutni broj bodova</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {standings.map((player, index) => (
            <TableRow key={index}>
              <TableCell>{index + 1}</TableCell>
              <TableCell>{player.imeprezime}</TableCell>
              <TableCell>{player.brbodovatrenutno}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default Poredak;
