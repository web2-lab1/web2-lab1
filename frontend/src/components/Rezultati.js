import React, { useState, useEffect } from 'react';
import { Table, TableContainer, TableHead, TableRow, TableCell, TableBody, Paper, Typography } from '@mui/material';
import Axios from 'axios';

const Rezultati = ({ natjecanjeid }) => {
  const [rezultati, setRezultati] = useState([]);
  const [naziv, setNaziv] = useState('');

  useEffect(() => {
    const fetchRezultati = async () => {
      try {
        const response = await Axios.get('https://web2-lab1-p5fl.onrender.com:8080/results', { params: { natjecanjeid } });
        console.log(response.data);
        setRezultati(response.data["rezultati"]);
        setNaziv(response.data["naziv"]["naziv"]);

      } catch (error) {
        console.error('Greška pri dohvaćanju rezultata:', error);
      }
    };

    fetchRezultati();
  }, []);

  return (
    <TableContainer component={Paper}>
        <Typography variant="h4" align="center" gutterBottom>{naziv}</Typography>
        <Table>
            <TableHead>
            <TableRow>
                <TableCell>Rbr Kola</TableCell>
                <TableCell>Rezultat 1</TableCell>
                <TableCell>Natjecatelj 1</TableCell>
                <TableCell>Natjecatelj 2</TableCell>
                <TableCell>Rezultat 2</TableCell>
            </TableRow>
            </TableHead>
            <TableBody>
            {rezultati.map((rezultat, index) => (
                <TableRow key={index}>
                  <TableCell>{rezultat.rbrkola}</TableCell>
                  <TableCell>{rezultat.rezultat1}</TableCell>
                  <TableCell>{rezultat.natjecatelj1}</TableCell>
                  <TableCell>{rezultat.natjecatelj2}</TableCell>
                  <TableCell>{rezultat.rezultat2}</TableCell>
                </TableRow>
            ))}
            </TableBody>
        </Table>
        </TableContainer>
  );
};

export default Rezultati;
