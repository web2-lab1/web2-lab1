import { Box, Button, Grid, Paper, TextField, Typography } from "@mui/material";
import { Formik, Field, Form } from "formik";
import Axios from 'axios';
import generator from 'tournament-generator';
import React, { useState } from 'react';
import { useAuth0 } from '@auth0/auth0-react';
import { useParams } from 'react-router-dom';


const CompetitionForm = () => {
  const [competitionLink, setCompetitionLink] = useState(null);
  const [natjecanjeID, setNatjecanjeID] = useState();
  const { code, state } = useParams();
  const { getAccessTokenSilently } = useAuth0();

  const initialValue = {
    naziv: "",
    sustav: "",
    pobjeda: "",
    remi: "",
    poraz: "",
    natjecatelji: "",
  };

  const handleSubmit = async (values, props) => {

    try {
      const response = await Axios.post('https://web2-lab1-p5fl.onrender.com:8080/create', values);

      console.log(response.data);

      // Resetiraj formu
      props.resetForm();

      const natjecatelji = response.data.natjecatelji;
      const natjecanjeID = response.data.natjecanjeid;
      setNatjecanjeID(natjecanjeID);

      console.log(natjecatelji);

      const { data: games } = generator(natjecatelji, { type: 'single-round'});
      console.log(games);

      const gamesResponse = await Axios.post('https://web2-lab1-p5fl.onrender.com:8080/schedule', {
        natjecanjeID, games
      });

      setCompetitionLink(`https://web2-lab1-front-e9ew.onrender.com:3000/competition/${natjecanjeID}`);

    } catch (error) {
      console.error('Greška pri slanju podataka:', error);
    }
  };

  return (
    <Grid container>
      <Grid item sm={3} xs={false}></Grid>
      <Grid item sm={6} xs={12}>
        <Paper>
          <Box m={5} p={3}>
            <Typography variant="h5">Stvori novo natjecanje</Typography>
            <Formik initialValues={initialValue} onSubmit={handleSubmit}>
              {(props) => {
                return (
                  <div>
                    <Form>
                      <TextField
                        label="Naziv"
                        name="naziv"
                        fullWidth
                        variant="outlined"
                        margin="dense"
                        value={props.values.naziv}
                        onChange={props.handleChange}
                        onBlur={props.handleBlur}
                        required
                      />
                      <Field
                        as={TextField}
                        label="Sistem natjecanja"
                        name="sustav"
                        fullWidth
                        variant="outlined"
                        margin="dense"
                        value={props.values.sustav}
                        onChange={props.handleChange}
                        onBlur={props.handleBlur}
                        required
                      />
                      <TextField
                        label="Bodovi za pobjedu"
                        type="number"
                        name="pobjeda"
                        fullWidth
                        variant="outlined"
                        margin="dense"
                        value={props.values.pobjeda}
                        onChange={props.handleChange}
                        onBlur={props.handleBlur}
                        required
                      />
                      <TextField
                        label="Bodovi za remi"
                        type="number"
                        name="remi"
                        fullWidth
                        variant="outlined"
                        margin="dense"
                        value={props.values.remi}
                        onChange={props.handleChange}
                        onBlur={props.handleBlur}
                        required
                      />
                      <TextField
                        label="Bodovi za poraz"
                        type="number"
                        name="poraz"
                        fullWidth
                        variant="outlined"
                        margin="dense"
                        value={props.values.poraz}
                        onChange={props.handleChange}
                        onBlur={props.handleBlur}
                        required
                      />
                      <TextField
                        label="Natjecatelji"
                        multiline
                        rows={4}
                        name="natjecatelji"
                        fullWidth
                        variant="outlined"
                        margin="dense"
                        value={props.values.natjecatelji}
                        onChange={props.handleChange}
                        onBlur={props.handleBlur}
                        required
                      />
                      <Button
                        variant="contained"
                        type="submit"
                        color="primary"
                        fullWidth
                      >
                        Submit
                      </Button>
                    </Form>
                    {competitionLink && (
                      <div>
                        <Typography variant="body1" color="textSecondary" style={{ marginTop: '10px' }}>
                          Pregledaj natjecanje na: <a href={competitionLink} target="_blank" rel="noopener noreferrer">{competitionLink}</a>
                        </Typography>
                      </div>
                    )}
                  </div>
                );
              }}
            </Formik>
          </Box>
        </Paper>
      </Grid>
      <Grid item sm={3} xs={false}></Grid>
    </Grid>
  );
};

export default CompetitionForm;
