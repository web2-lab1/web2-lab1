import React, { useState, useEffect } from 'react';
import { Table, TableContainer, TableHead, TableRow, TableCell, TableBody, Paper, Typography } from '@mui/material';
import Axios from 'axios';

const Raspored = ({natjecanjeid}) => {
  const [raspored, setRaspored] = useState([]);
  const [naziv, setNaziv] = useState('');

  useEffect(() => {
    const fetchRezultati = async () => {
      try {
        const response = await Axios.get('https://web2-lab1-p5fl.onrender.com:8080/schedule', { params: { natjecanjeid } });
        console.log(response.data);
        setRaspored(response.data["raspored"]);
        setNaziv(response.data["naziv"]["naziv"]);

      } catch (error) {
        console.error('Greška pri dohvaćanju rasporeda:', error);
      }
    };

    fetchRezultati();
  }, []);

  return (
    <TableContainer component={Paper}>
      <br /><br/ >
      <Typography variant="h4" align="center" gutterBottom>Raspored</Typography>
      <Table>
          <TableHead>
              <TableRow>
                  <TableCell>Rbr Kola</TableCell>
                  <TableCell>Natjecatelj 1</TableCell>
                  <TableCell>Natjecatelj 2</TableCell>
              </TableRow>
          </TableHead>
          <TableBody>
              {raspored.map((r, index) => (
                  <TableRow key={index}>
                      <TableCell>{r.rbrkola}</TableCell>
                      <TableCell>{r.natjecatelj1}</TableCell>
                      <TableCell>{r.natjecatelj2}</TableCell>
                  </TableRow>
              ))}
          </TableBody>
      </Table>
    </TableContainer>
  );
};

export default Raspored;
