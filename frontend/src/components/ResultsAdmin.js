import React, { useState, useEffect } from 'react';
import { Table, TableContainer, TableHead, TableRow, TableCell, TableBody, Paper, Typography, IconButton, Dialog, DialogTitle, DialogContent, DialogActions, Button, TextField } from '@mui/material';
import Axios from 'axios';
import EditIcon from '@mui/icons-material/Edit';
import AddIcon from '@mui/icons-material/Add';

const ResultsAdmin = ({natjecanjeid}) => {
    const [rezultati, setRezultati] = useState([]);
    const [raspored, setRaspored] = useState([]);
    const [naziv, setNaziv] = useState('');
    const [editRow, setEditRow] = useState(null);
    const [editValue, setEditValue] = useState('');
    const [editValue2, setEditValue2] = useState('');
    const [openDialog, setOpenDialog] = useState(false);
    const [openAddDialog, setOpenAddDialog] = useState(false);
    const [newResult1, setNewResult1] = useState('');
    const [newResult2, setNewResult2] = useState('');
    const [addDialogGame, setAddDialogGame] = useState(null); 

    const fetchRezultati = async () => {
        try {
        const responseRezultati = await Axios.get('https://web2-lab1-p5fl.onrender.com:8080/results', { params: { natjecanjeid } });
        setRezultati(responseRezultati.data["rezultati"]);

        const responseRaspored = await Axios.get('https://web2-lab1-p5fl.onrender.com:8080/schedule/filtered', { params: { natjecanjeid } });
        setRaspored(responseRaspored.data["raspored"]);

        setNaziv(responseRezultati.data["naziv"]["naziv"]);
        } catch (error) {
        console.error('Greška pri dohvaćanju rezultata ili rasporeda:', error);
        }
    };

    useEffect(() => {
        fetchRezultati();
    }, []);

    const handleEditClick = (row) => {
        setEditRow(row);
        setEditValue(row.rezultat1);
        setEditValue2(row.rezultat2);
        setOpenDialog(true);
    };

    const handleSaveClick = async () => {
        try {
        await Axios.put('https://web2-lab1-p5fl.onrender.com:8080/results', {
            rezultatid: editRow.rezultatid,
            rezultat1: editValue,
            rezultat2: editValue2,
            natjecanjeID: natjecanjeid,
            natjecatelj1ID: editRow.natjecatelj1ID,
            natjecatelj2ID: editRow.natjecatelj2ID
        });
        setOpenDialog(false);
        fetchRezultati();
        } catch (error) {
        console.error('Greška pri spremanju rezultata:', error);
        }
    };

    const handleOpenAddDialog = (game) => {
        setNewResult1('');
        setNewResult2('');
        setAddDialogGame(game);
        setOpenAddDialog(true);
    };

    const handleCloseAddDialog = () => {
        setOpenAddDialog(false);
    };

    const handleAddResultsClick = async () => {
        try {

        if (addDialogGame) {
            console.log(addDialogGame)
            await Axios.post('https://web2-lab1-p5fl.onrender.com:8080/results', {
            rezultat1: newResult1,
            rezultat2: newResult2, 
            natjecanjeID: natjecanjeid,
            natjecatelj1ID: addDialogGame.natjecatelj1id,
            natjecatelj2ID: addDialogGame.natjecatelj2id,
            rbrKola: addDialogGame.rbrkola, 
            rasporedid: addDialogGame.rasporedid
            });
            setOpenAddDialog(false);
            fetchRezultati();
        }
        } catch (error) {
        console.error('Greška pri dodavanju rezultata:', error);
        }
    };

    return (
        <div>
        <TableContainer component={Paper} style={{ marginBottom: '20px' }}>
            <br /><br />
            <Typography variant="h4" align="center" gutterBottom>Rezultati</Typography>
            <Table>
            <TableHead>
                <TableRow>
                <TableCell>Rbr Kola</TableCell>
                <TableCell>Rezultat 1</TableCell>
                <TableCell>Natjecatelj 1</TableCell>
                <TableCell>Natjecatelj 2</TableCell>
                <TableCell>Rezultat 2</TableCell>
                <TableCell>Uredi</TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {rezultati.map((rezultat) => (
                <TableRow key={rezultat.rezultatid}>
                    <TableCell>{rezultat.rbrkola}</TableCell>
                    <TableCell>{rezultat.rezultat1}</TableCell>
                    <TableCell>{rezultat.natjecatelj1}</TableCell>
                    <TableCell>{rezultat.natjecatelj2}</TableCell>
                    <TableCell>{rezultat.rezultat2}</TableCell>
                    <TableCell>
                    <IconButton onClick={() => handleEditClick(rezultat)}>
                        <EditIcon />
                    </IconButton>
                    </TableCell>
                </TableRow>
                ))}
            </TableBody>
            </Table>
            <Dialog open={openDialog} onClose={() => setOpenDialog(false)}>
            <DialogTitle>Uredi rezultate</DialogTitle>
            <DialogContent>
                <TextField
                label="Novi rezultat 1"
                value={editValue}
                onChange={(e) => setEditValue(e.target.value)}
                />
                <TextField
                label="Novi rezultat 2"
                value={editValue2}
                onChange={(e) => setEditValue2(e.target.value)}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={() => setOpenDialog(false)}>Odustani</Button>
                <Button onClick={handleSaveClick} color="primary">Spremi</Button>
            </DialogActions>
            </Dialog>
        </TableContainer>
        <br /><br />
        <TableContainer component={Paper}>
            <Typography variant="h4" align="center" gutterBottom>Dodaj rezultate</Typography>
            <Table>
            <TableHead>
                <TableRow>
                <TableCell>Rbr Kola</TableCell>
                <TableCell>Natjecatelj 1</TableCell>
                <TableCell>Natjecatelj 2</TableCell>
                <TableCell>Unesi rezultat</TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {raspored.map((game) => (
                <TableRow key={game.rasporedid}>
                    <TableCell>{game.rbrkola}</TableCell>
                    <TableCell>{game.natjecatelj1}</TableCell>
                    <TableCell>{game.natjecatelj2}</TableCell>
                    <TableCell>
                    {!game.rezultat && (
                        <IconButton onClick={() => handleOpenAddDialog(game)}>
                        <AddIcon />
                        </IconButton>
                    )}
                    </TableCell>
                </TableRow>
                ))}
            </TableBody>
            </Table>
            <Dialog open={openAddDialog} onClose={handleCloseAddDialog}>
            <DialogTitle>Dodaj rezultate</DialogTitle>
            <DialogContent>
                <TextField
                label="Rezultat 1"
                value={newResult1}
                onChange={(e) => setNewResult1(e.target.value)}
                />
                <TextField
                label="Rezultat 2"
                value={newResult2}
                onChange={(e) => setNewResult2(e.target.value)}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={handleCloseAddDialog}>Odustani</Button>
                <Button onClick={handleAddResultsClick} color="primary">
                Dodaj rezultat
                </Button>
            </DialogActions>
            </Dialog>
        </TableContainer>
        </div>
    );
};

export default ResultsAdmin;
