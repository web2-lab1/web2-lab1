import React from 'react';
import { useAuth0 } from '@auth0/auth0-react';
import { AppBar, Toolbar, Button, Typography} from '@mui/material';
import {Link} from 'react-router-dom';

const Header = () => {
    const { isAuthenticated, logout, user } = useAuth0();

    return (
        <AppBar position="static">
        <Toolbar>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            Natjecanja
            </Typography>
            {isAuthenticated ? (
            <>
                <Typography variant="subtitle1" sx={{ marginRight: 2 }}>
                Dobrodošao, {user.name}
                </Typography>
                <Button color="inherit" component={Link} to="/newCompetition" > 
                    Home
                </Button>
                <Button color="inherit" onClick={() => logout({ returnTo: window.location.origin })}>
                    Logout
                </Button>
            </>
            ) : (
            <div></div>
            )}
        </Toolbar>
        </AppBar>
    );
};

export default Header;
