const express = require('express');
const cors = require('cors');
const app = express();
const port = 8080;

const createRouter = require('./routes/createRouter');
const resultsRouter = require('./routes/resultsRouter');
const tablicaRouter = require('./routes/tablicaRouter');
const scheduleRouter = require('./routes/scheduleRouter');

app.use(cors());
app.use(express.json());
app.use('/create', createRouter);
app.use('/results', resultsRouter);
app.use('/table', tablicaRouter);
app.use('/schedule', scheduleRouter);

app.listen(port, () => {
    console.log(`Server je na portu ${port}`);
})