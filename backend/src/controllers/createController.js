const db = require('../db');

const addNewCompetition = async (req, res) => {
    const { naziv, pobjeda, remi, poraz, natjecatelji } = req.body;

    try {
        const newCompetition = await db.query('INSERT INTO natjecanje (naziv, pobjeda, remi, poraz) VALUES ($1, $2, $3, $4) RETURNING natjecanjeid', [naziv, pobjeda, remi, poraz]);

        const natjecanjeId = newCompetition.rows[0].natjecanjeid;

        let natjecateljiArray;

        if (natjecatelji.includes(';')) {
            natjecateljiArray = natjecatelji.split(';');
        } else {
            natjecateljiArray = natjecatelji.split('\n');
        }

        const insertedNatjecatelji = [];

        for (const natjecatelj of natjecateljiArray) {
            if (natjecatelj !== "" && natjecatelj !== " ") {
                const igrac = await db.query('INSERT INTO natjecatelj (imeprezime, brbodovatrenutno, natjecanjeid) VALUES ($1, 0, $2) RETURNING *', [natjecatelj, natjecanjeId]);
                insertedNatjecatelji.push({ id: igrac.rows[0].natjecateljid, imeprezime: igrac.rows[0].imeprezime });
            }   
        }
        
        res.json({ natjecanjeid: natjecanjeId, natjecatelji: insertedNatjecatelji });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
};

const getCompetition = async (req, res) => {
    const { natjecanjeid } = req.query;
    try {
        const natj = await db.query("select naziv from natjecanje where natjecanjeID = $1", [natjecanjeid]);
        const naziv = natj.rows[0];
        console.log(naziv);
        res.json(naziv);
    } catch (error) {
        console.log(error);
        res.status(500);
    }
} 

module.exports = { addNewCompetition, getCompetition };
