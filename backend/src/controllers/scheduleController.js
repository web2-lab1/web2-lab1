const db = require('../db');

const saveSchedule = async (req, res) => {
    const { natjecanjeID, games } = req.body;

    try {
        for (const game of games) {
            const rasp = await db.query("insert into raspored (rbrKola, natjecanjeID, natjecatelj1id, natjecatelj2id) values ($1, $2, $3, $4)", 
                                    [game["round"], natjecanjeID, game["awayTeam"]["id"], game["homeTeam"]["id"]]);
        }
        res.json({message: "ok"});
    } catch (error) {
        res.status(500);
        console.log(error);
    }

};

const getSchedule = async (req, res) => {
    const { natjecanjeid } = req.query;

    try {
        const rasp = await db.query("select rasporedid, rbrkola, n1.imeprezime as natjecatelj1, n2.imeprezime as natjecatelj2 from raspored join natjecatelj as n1 on natjecatelj1id = n1.natjecateljid join natjecatelj as n2 on natjecatelj2id = n2.natjecateljid where raspored.natjecanjeid = $1", [natjecanjeid]);
        const natj = await db.query("select naziv from natjecanje where natjecanjeID = $1", [natjecanjeid]);
        const naziv = natj.rows[0];
        res.json({raspored: rasp.rows, naziv})
    } catch (error) {
        console.log(error);
        res.status(500);
    }
}

const getFilteredSchedule = async (req, res) => {
    const { natjecanjeid } = req.query;

    try {
        const filtered = await db.query("SELECT raspored.rasporedid, raspored.rbrkola, n1.imeprezime AS natjecatelj1, raspored.natjecatelj1ID, raspored.natjecatelj2ID, n2.imeprezime AS natjecatelj2 FROM raspored JOIN natjecatelj AS n1 ON natjecatelj1id = n1.natjecateljid JOIN natjecatelj AS n2 ON natjecatelj2id = n2.natjecateljid LEFT JOIN rezultat ON raspored.rasporedid = rezultat.rasporedid WHERE raspored.natjecanjeid = $1 AND rezultat.rasporedid IS NULL", [natjecanjeid]);
        const natj = await db.query("select naziv from natjecanje where natjecanjeID = $1", [natjecanjeid]);
        const naziv = natj.rows[0];

        res.json({raspored: filtered.rows, naziv})
    } catch (error) {
        console.log(error);
        res.json({message: "error"});
    }
}

module.exports = { saveSchedule, getSchedule, getFilteredSchedule }
