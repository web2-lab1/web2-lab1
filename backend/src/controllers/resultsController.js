const db = require('../db');

const addNewResult = async (req, res) => {
    const { natjecanjeID, rbrKola, natjecatelj1ID, natjecatelj2ID, rezultat1, rezultat2, rasporedid } = req.body;

    try {
        const result = await db.query("INSERT INTO rezultat (natjecanjeID, rbrKola, natjecatelj1ID, natjecatelj2ID, rezultat1, rezultat2, rasporedid) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING *", 
                                        [natjecanjeID, rbrKola, natjecatelj1ID, natjecatelj2ID, rezultat1, rezultat2, rasporedid]);
        //dodaj bodove
        const win = await db.query("SELECT pobjeda FROM natjecanje WHERE natjecanjeID = $1", [natjecanjeID]);
        const pobjeda = win.rows[0]["pobjeda"];
        const tie = await db.query("SELECT remi FROM natjecanje WHERE natjecanjeID = $1", [natjecanjeID]);
        const remi = tie.rows[0]["remi"];
        const lose = await db.query("SELECT poraz FROM natjecanje WHERE natjecanjeID = $1", [natjecanjeID]);
        const poraz = lose.rows[0]["poraz"];

        var add1 = (await db.query("SELECT brbodovatrenutno FROM natjecatelj WHERE natjecateljID = $1", [natjecatelj1ID])).rows[0]["brbodovatrenutno"];
        var add2 = (await db.query("SELECT brbodovatrenutno FROM natjecatelj WHERE natjecateljID = $1", [natjecatelj2ID])).rows[0]["brbodovatrenutno"];
        if (rezultat1 == "pobjeda") add1 += pobjeda; 
        if (rezultat2 == "pobjeda") add2 += pobjeda;
        if (rezultat2 == "remi") add1 += remi; 
        if (rezultat2 == "remi") add2 += remi;
        if (rezultat1 == "poraz") add1 += poraz;
        if (rezultat2 == "poraz") add2 += poraz;
        const i1 = await db.query("UPDATE natjecatelj SET brbodovatrenutno = $1 WHERE natjecateljID = $2 RETURNING *", [add1, natjecatelj1ID]);
        const i2 = await db.query("UPDATE natjecatelj SET brbodovatrenutno = $1 WHERE natjecateljID = $2 RETURNING *", [add2, natjecatelj2ID]);

        res.json(result.rows);
                        
    } catch (error) {
        console.log(error);
        res.status(500);
    }
};

const updateResult = async (req, res) => {
    const { rezultatid, rezultat1, rezultat2, natjecanjeID } = req.body;

    try {
        const oldRes1 = (await db.query("SELECT rezultat1 from rezultat where rezultatid = $1", 
                            [rezultatid])).rows[0]["rezultat1"];
        const oldRes2 = (await db.query("SELECT rezultat2 from rezultat where rezultatid = $1", 
                            [rezultatid])).rows[0]["rezultat2"]
        const result = await db.query("UPDATE rezultat SET rezultat1 = $1, rezultat2 = $2 WHERE rezultatid = $3", 
                                        [rezultat1, rezultat2, rezultatid]);
        //prvo oduzmi bodove pa ih dodaj 
        const win = await db.query("SELECT pobjeda FROM natjecanje WHERE natjecanjeID = $1", [natjecanjeID]);
        const pobjeda = win.rows[0]["pobjeda"];
        const tie = await db.query("SELECT remi FROM natjecanje WHERE natjecanjeID = $1", [natjecanjeID]);
        const remi = tie.rows[0]["remi"];
        const lose = await db.query("SELECT poraz FROM natjecanje WHERE natjecanjeID = $1", [natjecanjeID]);
        const poraz = lose.rows[0]["poraz"];
        
        const natjecatelj1ID = (await db.query("select natjecatelj1id from rezultat where rezultatid = $1", [rezultatid])).rows[0]["natjecatelj1id"];
        const natjecatelj2ID = (await db.query("select natjecatelj2id from rezultat where rezultatid = $1", [rezultatid])).rows[0]["natjecatelj2id"];


        var add1 = (await db.query("SELECT brbodovatrenutno FROM natjecatelj WHERE natjecateljID = $1", [natjecatelj1ID])).rows[0]["brbodovatrenutno"];
        var add2 = (await db.query("SELECT brbodovatrenutno FROM natjecatelj WHERE natjecateljID = $1", [natjecatelj2ID])).rows[0]["brbodovatrenutno"];

        if (oldRes1 == "pobjeda") add1 -= pobjeda;
        if (oldRes2 == "pobjeda") add2 -= pobjeda; 
        if (oldRes1 == "remi") add1 -= remi;
        if (oldRes2 == "remi") add2 -= remi;

        if (rezultat1 == "pobjeda") add1 += pobjeda; 
        if (rezultat2 == "pobjeda") add2 += pobjeda;
        if (rezultat2 == "remi") add1 += remi; 
        if (rezultat2 == "remi") add2 += remi;
        if (rezultat1 == "poraz") add1 += poraz;
        if (rezultat2 == "poraz") add2 += poraz;

        const i1 = await db.query("UPDATE natjecatelj SET brbodovatrenutno = $1 WHERE natjecateljID = $2 RETURNING *", [add1, natjecatelj1ID]);
        const i2 = await db.query("UPDATE natjecatelj SET brbodovatrenutno = $1 WHERE natjecateljID = $2 RETURNING *", [add2, natjecatelj2ID]);
        res.json(result);
                        
    } catch (error) {
        console.log(error);
        res.status(500);
    }
}

const getResults = async (req, res) => {
    const { natjecanjeid } = req.query;
    console.log(natjecanjeid);

    try {
        const results = await db.query("select rezultatid, rbrkola, rezultat1, rezultat2, n1.imeprezime as natjecatelj1, n2.imeprezime as natjecatelj2 from rezultat join natjecatelj as n1 on natjecatelj1id = n1.natjecateljid join natjecatelj as n2 on natjecatelj2id = n2.natjecateljid where rezultat.natjecanjeid = $1", [natjecanjeid]);
        const natj = await db.query("select naziv from natjecanje where natjecanjeID = $1", [natjecanjeid]);
        const naziv = natj.rows[0];
        console.log(naziv);
        res.json({rezultati:results.rows, naziv});
    } catch (error) {
        console.log(error);
        res.status(500);
    }
}

module.exports = { addNewResult, updateResult, getResults }