const db = require('../db');

const getList = async (req, res) => {
    console.log(req.query);
    const { natjecanjeid } = req.query;

    try {
        console.log(natjecanjeid);
        const result = await db.query("select * from natjecatelj where natjecanjeid = $1 order by brbodovatrenutno desc, imeprezime asc", [natjecanjeid]);
        const natj = await db.query("select naziv from natjecanje where natjecanjeID = $1", [natjecanjeid]);
        const naziv = natj.rows[0];
        res.json({poredak: result.rows, naziv});
    } catch (error) {
        console.log(error);
        res.status(500);
    }
};

module.exports = { getList }