const express = require('express');
const router = express.Router();
const resultsController = require('../controllers/resultsController');

router.post('/',  resultsController.addNewResult);
router.put('/', resultsController.updateResult);
router.get('/', resultsController.getResults);

module.exports = router;
