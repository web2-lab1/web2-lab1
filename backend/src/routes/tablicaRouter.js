const express = require('express');
const router = express.Router();
const tablicaController = require('../controllers/tablicaController');

router.get('/', tablicaController.getList);

module.exports = router;
