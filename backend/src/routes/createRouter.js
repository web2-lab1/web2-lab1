const express = require('express');
const router = express.Router();
const createController = require('../controllers/createController');
const { expressjwt: jwt } = require("express-jwt");

router.post('/', (req, res) => {
    try {
      createController.addNewCompetition(req, res);
    } catch (error) {
      console.error('Error processing request:', error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  });
router.get('/', createController.getCompetition);

module.exports = router;
