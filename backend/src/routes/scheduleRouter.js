const express = require('express');
const router = express.Router();
const scheduleController = require('../controllers/scheduleController');

router.post('/', scheduleController.saveSchedule);
router.get('/', scheduleController.getSchedule);
router.get('/filtered', scheduleController.getFilteredSchedule);

module.exports = router;
